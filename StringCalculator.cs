using System;


public static string operString = "*/+-";

public static int GetPrecedence(string contents)
{
    int precedence = 0;
    switch (contents)
    {
        case "+":
            precedence = 1;
            break;
        case "-":
            precedence = 1;
            break;
        case "*":
            precedence = 2;
            break;
        case "/":
            precedence = 2;
            break;
    }
    return precedence;
}

public static decimal CalculateOperation(List<string> operation)
{
    decimal result = 0;
    decimal firstNum = decimal.Parse(operation[0]);
    string oper = operation[1];
    decimal secondNum = decimal.Parse(operation[2]);

    switch (oper)
    {
        case "+":
            result = firstNum + secondNum;
            break;
        case "-":
            result = firstNum - secondNum;
            break;
        case "*":
            result = firstNum * secondNum;
            break;
        case "/":
            result = firstNum / secondNum;
            break;
    }

    return result;
}

public static string RemoveWhiteSpaces(string input)
{
    return new string(input.ToCharArray().Where(c => !Char.IsWhiteSpace(c)).ToArray());
}

//to break Equation with Parenthesis into separated Equation 
public static List<string> ParenthesisBreak(string input)
{
    List<string> operation = new List<string>();
    int leftParenthesisIndex = -1;
    int rightParenthesisIndex = -1;
    int nestedCount = 0;
    bool containParenthesis = input.Contains("(") || input.Contains(")");

    if (!containParenthesis)
    {
        operation.Add(input);
    }

    while (containParenthesis)
    {
        nestedCount = 0;
        rightParenthesisIndex = -1;
        leftParenthesisIndex = input.IndexOf("(");

        for (int i = leftParenthesisIndex + 1; i < input.Length; i++)
        {
            if (input[i] == ')')
            {
                if (nestedCount == 0)
                {
                    rightParenthesisIndex = i;
                }
                else
                {
                    nestedCount--;
                }
            }
            else if (input[i] == '(')
            {
                nestedCount++;
            }
        }
        
        if (leftParenthesisIndex != -1 && rightParenthesisIndex != -1)
        {
            if (leftParenthesisIndex == 0 && rightParenthesisIndex == input.Length - 1)
            {
                input = input.Substring(leftParenthesisIndex + 1, input.Length - 2);
            }
            else if (leftParenthesisIndex != 0)
            {
                operation.Add(input.Substring(0, leftParenthesisIndex));
                input = input.Remove(0, leftParenthesisIndex);
            }
            else
            {
                operation.Add(input.Substring(leftParenthesisIndex + 1, (rightParenthesisIndex - 1 - leftParenthesisIndex)));
                input = input.Remove(leftParenthesisIndex, rightParenthesisIndex - leftParenthesisIndex + 1);
            }
        }
        else if ((leftParenthesisIndex != -1 && rightParenthesisIndex == -1) ||
                (leftParenthesisIndex == -1 && rightParenthesisIndex != -1))
        {
            Console.WriteLine("unbalanced parenthesis.");
            break;
        }
        else
        {

        }

        containParenthesis = input.Contains("(") || input.Contains(")");
        if (!string.IsNullOrEmpty(input) && !containParenthesis)
        {
            operation.Add(input);
        }
    }
    
    return operation;
}

//to break Equation into List for each numbers and Operator
public static List<string> GetOperatorAndNumberList(string input)
{
    List<string> operatorAndNumberList = new List<string>();
    int lastOperatorIndex = 0;
    
    for (int i = 0; i < input.Length; i++)
    {
        if (operString.Contains(input[i]))
        {
            int substringStart = (i == 0 || lastOperatorIndex == 0) ? 0 : (lastOperatorIndex + 1);
            int substringLength = (i == 0) ? 1 : (i - (lastOperatorIndex + 1));
            if (lastOperatorIndex == 0 && i == 0)
            {
                operatorAndNumberList.Add(input.Substring(i, 1));
                lastOperatorIndex = i;
            }
            else if (lastOperatorIndex == 0 && i != 0)
            {
                operatorAndNumberList.Add(input.Substring(0, i));
                operatorAndNumberList.Add(input.Substring(i, 1));
                lastOperatorIndex = i;
            }
            else if ((i == input.Length - 1) && lastOperatorIndex == 0)
            {
                operatorAndNumberList.Add(input.Substring(0, i));
                operatorAndNumberList.Add(input.Substring(i, 1));
                lastOperatorIndex = i;
            }
            else if ((i == input.Length - 1))
            {
                operatorAndNumberList.Add(input.Substring(substringStart, i - (lastOperatorIndex + 1)));
                operatorAndNumberList.Add(input.Substring(i, 1));
                lastOperatorIndex = i;
            }
            else
            {

                operatorAndNumberList.Add(input.Substring(substringStart, substringLength));
                operatorAndNumberList.Add(input.Substring(i, 1));
                lastOperatorIndex = i;
            }
        }
        else if (i == input.Length - 1 && lastOperatorIndex != i)
        {
            operatorAndNumberList.Add(input.Substring(lastOperatorIndex + 1, i - lastOperatorIndex));
        }
    }
    return operatorAndNumberList;
}


//recursive function to solve OperationList until a single number or a number with operator is get
public static List<string> ProcessOperationList(List<string> operationList)
{
    int operationStartIndex = operString.Contains(operationList[0]) ? 1 : 0;
    if (operationList.Count >= operationStartIndex + 5)
    {

        //operation of 5 items with left right logic 
        //resolve left operation first
        if (GetPrecedence(operationList[operationStartIndex + 1]) >= GetPrecedence(operationList[operationStartIndex + 3]))
        {
            //resolve left operation
            List<string> operationForCalculate = operationList.GetRange(operationStartIndex, 3);
            decimal operationResult = CalculateOperation(operationForCalculate);

            //resolve right operation
            operationForCalculate = new List<string>() { operationResult.ToString() };
            operationForCalculate.AddRange(operationList.GetRange(operationStartIndex + 3, 2));
            operationResult = CalculateOperation(operationForCalculate);

            operationList[operationStartIndex + 4] = operationResult.ToString();
            operationList.RemoveRange(operationStartIndex, 4);

        }
        //resolve right operation first
        else
        {
            //resolve right operation
            List<string> operationForCalculate = operationList.GetRange(operationStartIndex + 2, 3);
            decimal operationResult = CalculateOperation(operationForCalculate);

            //resolve left operation
            operationForCalculate = new List<string>();
            operationForCalculate.AddRange(operationList.GetRange(operationStartIndex, 2));
            operationForCalculate.Add(operationResult.ToString());
            operationResult = CalculateOperation(operationForCalculate);

            //reassign and remove operationList
            operationList[operationStartIndex + 4] = operationResult.ToString();
            operationList.RemoveRange(operationStartIndex, 4);
        }
    }
    else
    {
        if (operationList.Count >= operationStartIndex + 3)
        {
            //operation of 3 items
            List<string> operationForCalculate = operationList.GetRange(operationStartIndex, 3);
            decimal operationResult = CalculateOperation(operationForCalculate);

            //reassign and remove operationList
            operationList[operationStartIndex + 2] = operationResult.ToString();
            operationList.RemoveRange(operationStartIndex, 2);
        }
    }

    int operationListCount = operationList.Count;
    if (operationListCount > 2)
    {
        if (!(operationListCount == 3 && (operString.Contains(operationList[0]) && operString.Contains(operationList[2]))))
        {
            operationList = ProcessOperationList(operationList);
        }
    }
    else if (operationList.Count == 2)
    {
        operationList[0] += operationList[1];
    }

    return operationList;
}


public static double Evaluate(List<string> OperationList)
{
    double result = 0;
    
    //loop to resolve each equation in OperationList
    for (int i = 0; i < OperationList.Count; i++)
    {
        OperationList[i] = ProcessOperationList(GetOperatorAndNumberList(OperationList[i]))[0];
    }

    //resolve the final OperationList
    result = double.Parse((OperationList.Count > 1) ? ProcessOperationList(GetOperatorAndNumberList(string.Join("", OperationList.ToArray())))[0] : OperationList[0]);

    return result;
}

public static double Calculate(string sum)
{
    double output = 0;

    sum = RemoveWhiteSpaces(sum);
    List<string> operation = ParenthesisBreak(sum);
    output = Evaluate(operation);

    return output;
}


Console.WriteLine("1 + 1 = " + Calculate("1 + 1").ToString());
Console.WriteLine("2 * 2 = " + Calculate("2 * 2").ToString());
Console.WriteLine("1 + 2 + 3 = " + Calculate("1 + 2 + 3").ToString());
Console.WriteLine("1 + 2 * 3 = " + Calculate("1 + 2 * 3").ToString());
Console.WriteLine("6 / 2 = " + Calculate("6 / 2").ToString());
Console.WriteLine("11 + 23 = " + Calculate("11 + 23").ToString());
Console.WriteLine("11.1 + 23 = " + Calculate("11.1 + 23").ToString());
Console.WriteLine("23 - ( 29.3 - 12.5 ) = " + Calculate("23 - ( 29.3 - 12.5 )").ToString());
Console.WriteLine("( 11.5 + 15.4 ) + 10.1 = " + Calculate("( 11.5 + 15.4 ) + 10.1").ToString());